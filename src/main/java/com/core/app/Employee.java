package com.core.app;

public class Employee {
	private int empId;
	private String ename;
	private double salary;
	private String location;
	private String company;
	
	public Employee() {
		
	}
	

	public Employee(int empId, String ename, double salary, String location, String company) {
		
		this.empId = empId;
		this.ename = ename;
		this.salary = salary;
		this.location = location;
		this.company = company;
	}

	@Override
	public String toString() {
		return "Employee [empId=" + empId + ", ename=" + ename + ", salary=" + salary + ", location=" + location
				+ ", company=" + company + "]";
	}


	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public int getEmpId() {
		return empId;
	}

	public void setEmpId(int empId) {
		this.empId = empId;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	

	public void add(int a, int b) {
		System.out.println(a + b);
	}

	public int multi(int a, int b) {
		return a * b;
	}

	public boolean permCont(String emp) {
		if (emp == "TCS") {
			return true;
		} else {
			return false;
		}

	}

}
