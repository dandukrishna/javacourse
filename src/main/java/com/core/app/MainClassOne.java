package com.core.app;

public class MainClassOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	
		//   -----   01 -----------------------
		//Calling the Class file in main method by using the new key of Object;
		ExampleOne OneObject = new ExampleOne();
		
		//Set the value to the method of ExampleOne class
		int  E_OneE = OneObject.StringLength("Krishna Dandu");
		
		System.out.println(E_OneE);
		
		
		
		///   -----   02 -----------------------		
		// Method to find a given number is even or odd	
		
		String WhichNumber = OneObject.FindWhichNumber(191);
		System.out.println(WhichNumber);
		
		///   -----   02-01 -----------------------
		int a[] = {1,2,5,6,3,2,200,1000,99};
		OneObject.FindWhichNumber(a);
		//OneObject.FindWhichNumberTwo(a);
		
		
		///   -----   03 ---------------------------
		// write a method to find square of a number
		int b[] = {1,2,5,6,3,2,200,1000,99};
		String SqRootNum = OneObject.FindSqNumber(b);
	}

}
