package com.core.app;

public class EmployeeDetails {	
	private int psId ;
	private String name;
	private int age;
	private String location;
	private double salary;
	public String address;
	
	@Override
	public String toString() {
		return "EmployeeDetails [psId=" + psId + ", name=" + name + ", age=" + age + ", location=" + location
				+ ", salary=" + salary + "]";
	}

	
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPsId() {
		return psId;
	}

	public void setPsId(int psId) {
		this.psId = psId;
	}
	
	public void getAddres(){
		
	}
	
	
	
	
}